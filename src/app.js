const STEP_DURATION = 2000;
const ACCELERATION = 0.002;
const MAX_ARC_SIZE = 0.8;
const LINE_WIDTH = 0.5;
const STROKE_STYLE = "#333";
const NUMBER_FORMAT = new Intl.NumberFormat("en-US");
const MAX_SYNC_TASK_DURATION = 10;

document.addEventListener("DOMContentLoaded", main);

function main() {
  const currentTime = now();
  advanceAnimation({
    sequence: [],
    startTime: currentTime,
    frameStartTime: currentTime,
    lastKeyframe: null,
    nextKeyframe: { time: currentTime, shift1000x: null, scale: null },
    canvas: document.querySelector("canvas"),
    display: document.querySelector("h1"),
    secondaryDisplay: document.querySelector("h2"),
  });
}

function now() {
  return Date.now();
}

function advanceAnimation(state) {
  const { lastKeyframe, nextKeyframe, canvas } = state;
  const frameStartTime = now();
  const nextState = { ...state, frameStartTime };
  adjustCanvasSize(canvas);
  if (lastKeyframe === null || frameStartTime >= nextKeyframe.time) {
    advanceSequence(nextState);
  } else {
    repaintAnimation(nextState);
  }
}

function adjustCanvasSize(canvas) {
  canvas.width = canvas.clientWidth;
  canvas.height = canvas.clientHeight;
}

function advanceSequence(state) {
  const { startTime, frameStartTime, sequence, canvas, nextKeyframe } = state;
  let stepTime;
  do {
    stepTime = calculateStepTime(sequence.length, startTime);
    appendNextRecamanNumber(sequence);
  } while (
    stepTime <= frameStartTime &&
    now() - frameStartTime <= MAX_SYNC_TASK_DURATION
  );
  const lastTwoNumbers = sequence.slice(-2);
  const nextState = {
    ...state,
    lastKeyframe: nextKeyframe,
    nextKeyframe: calculateKeyframe(lastTwoNumbers, stepTime, canvas),
  };
  repaintAnimation(nextState);
  updateDisplays(nextState);
}

function calculateStepTime(stepIndex, startTime) {
  const stepCoefficient = Math.min(stepIndex, 1 / ACCELERATION);
  const accCoefficient = 1 - ACCELERATION * 0.5 * (stepCoefficient + 1);
  const timeout = STEP_DURATION * stepCoefficient * accCoefficient;
  return startTime + timeout;
}

function appendNextRecamanNumber(sequence) {
  const nextNumber = calculateNextRecamanNumber(sequence);
  sequence.push(nextNumber);
}

function calculateNextRecamanNumber(sequence) {
  const lastNumber = sequence.length > 0 ? sequence[sequence.length - 1] : 0n;
  const distance = BigInt(sequence.length);
  const lowCandidate = lastNumber - distance;
  return sequence.includes(lowCandidate) || lowCandidate < 0
    ? lastNumber + distance
    : lowCandidate;
}

function calculateKeyframe(lastTwoNumbers, time, canvas) {
  const center1000x = calculateArcCenter1000x(...lastTwoNumbers);
  const radius10x = calculateArcRadius10x(...lastTwoNumbers);
  const desiredArcRadius = calculateDesiredRadius(canvas);
  const shift1000x = -center1000x;
  const scale = (desiredArcRadius * 10) / Number(radius10x);
  return { time, shift1000x, scale };
}

function calculateArcCenter1000x(numberA, numberB) {
  return ((numberA + numberB) * 1000n) / 2n;
}

function calculateArcRadius10x(numberA, numberB) {
  return (absoluteValue(numberA - numberB) * 10n) / 2n;
}

function absoluteValue(number) {
  return number > 0 ? number : -number;
}

function calculateDesiredRadius(canvas) {
  return (Math.min(canvas.width, canvas.height) * MAX_ARC_SIZE) / 2;
}

function repaintAnimation(state) {
  const { frameStartTime, lastKeyframe, nextKeyframe } = state;
  const animationData = interpolateKeyframes(
    lastKeyframe,
    nextKeyframe,
    frameStartTime
  );
  renderVisual(state, animationData);
  window.requestAnimationFrame(advanceAnimation.bind(null, state));
}

function interpolateKeyframes(lastKeyframe, nextKeyframe, frameStartTime) {
  const progress = calculateAnimationProgress(
    lastKeyframe.time,
    nextKeyframe.time,
    frameStartTime
  );
  const shift1000x = interpolateBigIntValue(
    lastKeyframe.shift1000x,
    nextKeyframe.shift1000x,
    progress
  );
  const scale = interpolateValue(
    lastKeyframe.scale,
    nextKeyframe.scale,
    progress
  );
  return { progress, shift1000x, scale };
}

function calculateAnimationProgress(lastTime, nextTime, frameStartTime) {
  return Math.min(1, (frameStartTime - lastTime) / (nextTime - lastTime));
}

function interpolateBigIntValue(from, to, progress) {
  if (from === null) {
    return to;
  }
  return from + BigInt(Math.round(Number(to - from) * progress));
}

function interpolateValue(from, to, progress) {
  if (from === null) {
    return to;
  }
  return from + (to - from) * progress;
}

function renderVisual(state, animationData) {
  const { sequence, canvas } = state;
  prepareCanvas(canvas);
  renderArcs(sequence, animationData, canvas);
}

function prepareCanvas(canvas) {
  canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
}

function renderArcs(sequence, animationData, canvas) {
  const canvasContext = canvas.getContext("2d");
  canvasContext.lineWidth = LINE_WIDTH;
  canvasContext.strokeStyle = STROKE_STYLE;
  canvasContext.beginPath();
  for (let i = 0; i < sequence.length - 1; i++) {
    const pair = sequence.slice(i, i + 2);
    const direction = calculateArcDirection(i);
    const last = i === sequence.length - 2;
    renderArc(pair, direction, last, animationData, canvas);
  }
  canvasContext.stroke();
}

function calculateArcDirection(firstNumberIndex) {
  return firstNumberIndex % 2 === 0 ? 1 : -1;
}

function renderArc(pair, direction, last, animationData, canvas) {
  const { progress, shift1000x, scale } = animationData;
  const finalShift = canvas.width / 2;
  const pointA = transformNumber(pair[0], shift1000x, scale, finalShift);
  const pointB = transformNumber(pair[1], shift1000x, scale, finalShift);
  if (isArcOutOfCanvas(pointA, pointB, canvas)) {
    return;
  }
  const effectiveProgress = last ? progress : 1;
  renderArcOfPoints(pointA, pointB, direction, effectiveProgress, canvas);
}

function transformNumber(number, shift1000x, scale, finalShift) {
  return (Number(number * 1000n + shift1000x) / 1000) * scale + finalShift;
}

function isArcOutOfCanvas(pointA, pointB, canvas) {
  return (
    (pointA < 0 && pointB < 0) ||
    (pointA > canvas.width && pointB > canvas.width)
  );
}

function renderArcOfPoints(pointA, pointB, direction, progress, canvas) {
  const x = (pointA + pointB) / 2;
  const y = canvas.height / 2;
  const radius = Math.abs(pointA - pointB) / 2;
  const angles =
    pointA < pointB
      ? [Math.PI, Math.PI * (1 + direction * progress)]
      : [0, Math.PI * -direction * progress];
  const anticlockwise = xor(pointA < pointB, direction === 1);
  canvas.getContext("2d").arc(x, y, radius, ...angles, anticlockwise);
}

function xor(a, b) {
  return a !== b;
}

function updateDisplays(state) {
  const {
    startTime,
    frameStartTime,
    sequence,
    display,
    secondaryDisplay,
  } = state;
  const lastVisitedNumber = sequence[sequence.length - 2];
  const stepIndex = sequence.length - 1;
  const currentTime = now();
  const frameTime = currentTime - frameStartTime;
  const totalTime = currentTime - startTime;
  showNumber(display, lastVisitedNumber);
  showDetails(secondaryDisplay, stepIndex, frameTime, totalTime);
}

function showNumber(display, number) {
  display.textContent = formatNumber(number);
}

function formatNumber(number) {
  return NUMBER_FORMAT.format(number);
}

function showDetails(display, stepIndex, frameTime, totalTime) {
  const entries = [
    `#${formatNumber(stepIndex)}`,
    `${frameTime}ms`,
    `${formatNumber(Math.floor(totalTime / 1000))}s`,
  ];
  display.textContent = entries.join(" | ");
}
