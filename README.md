# Recamán's Sequence

This is a simple visualizatin of the Recamán's number sequence as described in
The On-Line Encyclopedia of Integer Sequences (OEIS).

The visualization is created using JavaScript and canvas element. It should
work in all the modern browsers.

Open `src/index.html` to start the visualization.

# References

- Description of the Recamán's sequence in OEIS: https://oeis.org/A005132
- Video about the sequence: https://www.youtube.com/watch?v=FGC5TdIiT9U
